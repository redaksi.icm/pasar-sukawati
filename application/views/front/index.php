<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/template_front/img/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon"
          href="<?php echo base_url() ?>assets/template_front/img/favicon.png">
    <link href="<?php echo base_url() ?>assets/template_front/css/bootstrap_customized.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/template_front/css/home.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/template_front/css/listing.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/custom.css" rel="stylesheet">

</head>

<body>

<header class="header_in clearfix element_to_stick">
    <?php echo $menu ?>
</header>

<?php echo $content ?>
<?php echo $footer ?>

<div id="toTop"></div>

<div class="layer"></div>

<script src="<?php echo base_url() ?>assets/template_front/js/common_scripts.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/slider.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/common_func.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/assets/validate.js"></script>

</body>
</html>